import Vue from 'vue'
import Vuex from 'vuex';
import appStore from './modules/app'
import menuStore from './modules/menu'


Vue.use(Vuex);


const createStore = () => {
  return new Vuex.Store({
    namespaced: true,
    modules: {
      app: appStore,
      menu: menuStore,
    },
  });
};


/*

export const state = () => ({
  drawer: true
})

export const mutations = {
  toggleDrawer(state) {
    state.drawer = !state.drawer
  },
  drawer(state, val) {
    state.drawer = val
  }
}
*/


export default createStore

